---
layout: default
date: 15-11-2020
short_title: Colibri2
title: Development of Colibri2
logo: /assets/images/colibri/logo/colibri2_optimized_pink.svg
---

The learning developed in Popop is removed to keep only the propagation part as
in COLIBRI. So the solver is named Colibri2. The first features to be available
are:
 * Boolean
 * Linear arithmetic (using Fourier-Mostkin)
 * Uninterpreted symbols
 * Quantifiers
