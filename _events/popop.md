---
layout: default
date: 29-03-2016
short_title: Popop
title: Framework combining SMT and CP
link: https://soprano-project.fr
---

Popop, developped during the SOPRANO project, is a framework that combined the
learning of SMT and the propagation of CP. However the learning framework was to
cumbersome to allow the developpement of more complicated theories than booleans
and linear arithmetic.
