---
layout: default
date: 15-01-2019
short_title: Colibrics
title: Development of Colibrics
logo: /assets/images/colibri/logo/colibri2_optimized_blue.svg
---

Colibrics started as an experiment on the possibility to prove a simple CP
solver in Why3
