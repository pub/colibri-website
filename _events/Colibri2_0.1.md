---
layout: default
date: 20-08-2021
short_title: Colibri2 0.1
title: First public release of Colibri2
logo: /assets/images/colibri/logo/colibri2_optimized_pink.svg
---

The solver supports:
 * Boolean
 * Linear arithmetic (using Fourier-Mostkin)
 * Uninterpreted symbols
 * Quantifiers
 * Polymorphism

In a limited way:
 * Floating Point numbers
 * Function ceil and floor
