---
layout: default
date: 01-01-2014
short_title: SOPRANO
title: Start of the SOPRANO project
link: https://soprano-project.fr
---

The [ANR](https://anr.fr/Project-ANR-14-CE28-0020) [SOPRANO
project](http://soprano-project.fr) allowed to gather researchers working on SMT
(OcamlPro, Université Paris-Saclay) and CP (CEA, INRIA) and industrials partner
(Adacore) in the domain of software verification. The goal was to merge the
powerful learning of SMT and the powerful propagations of CP.
