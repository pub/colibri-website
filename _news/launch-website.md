---
date: 01/01/2023 00:00
title: Website launched
logo: assets/images/pyrat-logo.png
layout: new
---

The PyRAT website is officially online! Regular updates and news will be put in this website to inform of the latest progress in the tool.

To learn more about the tool visit the main page or <a href="{{ '/contact.html' | relativize_url }}">contact us</a>
