---
layout: default
title: Colibri, Colibri2, Colibrics -- Installation
direct: true
---

## Installation

### Static binaries

#### **COLIBRI**

  Download a `bundle` directory which contains the `colibri` executable and its dependencies.

  - [Latest release](https://git.frama-c.com/pub/colibri/-/releases/permalink/latest/downloads/bundle-v7)
  - [Latest release (with ECLiPSe 5.10)](https://git.frama-c.com/pub/colibri/-/releases/permalink/latest/downloads/bundle-v5)
  - [All releases](https://git.frama-c.com/pub/colibri/-/releases/)
  - [Nightly jobs](https://git.frama-c.com/pub/colibri/-/pipelines?scope=finished&source=schedule&status=success)

#### **Colibri2**

  - [Colibri2 0.4](https://git.frama-c.com/pub/colibrics/-/jobs/artifacts/0.4/raw/bin/colibri2/colibri2?job=generate-static:%20[4.12.0])
  - [Colibri2 development version](https://git.frama-c.com/pub/colibrics/-/jobs/artifacts/master/raw/bin/colibri2/colibri2?job=generate-static:%20[4.14.0])

### From source

#### **COLIBRI**

   - [Gitlab repository](https://git.frama-c.com/pub/colibri/)

#### **Colibri2**

  - [`opam install colibri2`](https://ocaml.org/p/colibri2/latest)
  - [`opam install colibrics`](https://ocaml.org/p/colibrics/latest)
