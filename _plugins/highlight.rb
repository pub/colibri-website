# frozen_string_literal: true

module Jekyll
  module Tags
    class HighlightBlock2 < Jekyll::Tags::HighlightBlock

      #use a div instead of a figure for compatibility with bulma
      def add_code_tag(code)
        code_attrs = %(class="language-#{@lang.tr("+", "-")}" data-lang="#{@lang}")
        %(<div class="highlight"><pre><code #{code_attrs}>#{code.chomp}</code></pre></figure>)
      end
    end
  end
end

Liquid::Template.register_tag("highlight2", Jekyll::Tags::HighlightBlock2)
